from curses import *
from curses import panel
import sprites

def draw_board(win, MAP):
    win.clear()
    for i in range(len(MAP)):
        for j in range(len(MAP[i])):
            tile = MAP[i][j]
            if tile == sprites.block:
                win.attron(color_pair(1))
                win.addstr(i, j, tile)
                win.attroff(color_pair(1))
            elif tile == sprites.hazard:
                win.attron(color_pair(2))
                win.addstr(i, j, tile)
                win.attroff(color_pair(2))
            elif tile == sprites.forward or tile == sprites.backward:
                win.attron(color_pair(3))
                win.addstr(i, j, tile)
                win.attroff(color_pair(3))
            elif tile == sprites.end_level:
                win.attron(color_pair(4))
                win.addstr(i, j, tile)
                win.attroff(color_pair(3))
            elif tile == sprites.flipper:
                win.addstr(i, j, tile)

def load_board(f, win, MAP):
    LEVEL = open(f)
    MAP = LEVEL.readlines()
    draw_board(win, MAP)
    LEVEL.close()
    update()

    return MAP

def update():
    panel.update_panels()
    doupdate