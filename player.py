from curses import *
from curses import panel
import sprites

def update_pos(key, pan, y, x, f):
    if key == KEY_LEFT:
        x = 29
    elif key == KEY_RIGHT:
        x = 2
    elif key == KEY_UP or f ==True:
        y = 18
    elif key == KEY_DOWN or f == False:
        y = 3

    pan.move(y, x)
    return (y, x)

def gravity(pan, y, x):
    pan.move(y, x)