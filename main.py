from curses import *
from curses import panel
import sprites
import checks
import player
import draw

def move(pan, y, x, f):
    global p_y, p_x
    if f == False:
        check = CURRENT_MAP[y+1][x]
    else:
        check = CURRENT_MAP[y][x]

    if check != sprites.block:
        p_y = y
        p_x = x
        pan.move(p_y, p_x)

stdscr = initscr()
start_color()
noecho()
curs_set(False)
stdscr.keypad(True)

init_pair(1, COLOR_MAGENTA, COLOR_CYAN)
init_pair(2, COLOR_RED, COLOR_YELLOW)
init_pair(3, COLOR_WHITE, COLOR_GREEN)
init_pair(4, COLOR_BLUE, COLOR_BLACK)

CURRENT_MAP = []
board_num = 1
coord_num = 1

COORD = open("COORD" + str(coord_num))
player_pos = COORD.readlines()
p_y = int(player_pos[0])
p_x = int(player_pos[1])
COORD.close()

board_window = newwin(21, 33, 0, 0)
CURRENT_MAP = draw.load_board("MAPS/BOARD" + str(board_num), board_window, CURRENT_MAP)
board_pan = panel.new_panel(board_window)

player_win = newwin(2, 1, p_y, p_x)
player_win.bkgd(sprites.player)
player_panel = panel.new_panel(player_win)
panel.update_panels()
doupdate()

stdscr.addstr(25, 1, "Esc to quit, Left for left, Right for Right, Up to fall up, Down to fall down")

dead = False
won = False
falling = False

game_loop = True 
while(game_loop):
    stdscr.clear()
    stdscr.addstr(26, 1, "y = " + str(falling))


    panel.update_panels()
    doupdate()

    if falling:
        move(player_panel, p_y - 1, p_x, falling) 
    elif not falling:
        move(player_panel, p_y + 1, p_x, falling)

    falling = checks.flipper_check(falling, p_y, p_x, CURRENT_MAP)
    
    won = checks.winning_check(p_y, p_x, CURRENT_MAP)
    if won:
        board_window.clear()
        CURRENT_MAP = draw.load_board("MAPS/WON", board_window, CURRENT_MAP)
        game_loop = False
        break

    dead = checks.hazard_check(p_y, p_x, CURRENT_MAP)
    if dead:
        board_window.clear()
        CURRENT_MAP = draw.load_board("MAPS/DEATH", board_window, CURRENT_MAP)
        game_loop = False
        break

    if checks.next_board(p_y, p_x, CURRENT_MAP):
        board_window.clear()
        p_y, p_x = player.update_pos(key, player_panel, p_y, p_x, falling)
        board_num += 1
        CURRENT_MAP = draw.load_board("MAPS/BOARD" + str(board_num), board_window, CURRENT_MAP)
    elif checks.prev_board(p_y, p_x, CURRENT_MAP):
        board_window.clear()
        p_y, p_x = player.update_pos(key, player_panel, p_y, p_x, falling)
        board_num -= 1
        CURRENT_MAP = draw.load_board("MAPS/BOARD" + str(board_num), board_window, CURRENT_MAP)


    panel.update_panels()
    doupdate()

    key = stdscr.getch()        
    if key == 27:
        game_loop = False
        break
    elif key == KEY_LEFT:
        move(player_panel, p_y, p_x - 1, falling)
    elif key == KEY_RIGHT:
        move(player_panel, p_y, p_x + 1, falling)    
    elif key == KEY_UP and CURRENT_MAP[p_y + 2][p_x] == sprites.block:
        falling = True
    elif key == KEY_DOWN and CURRENT_MAP[p_y - 1][p_x] == sprites.block:
        falling = False

    

panel.update_panels()
doupdate()

while dead:
    key = stdscr.getch()
    if key == 27:
        dead = False
        break

while won:
    key = stdscr.getch()
    if key == 27:
        won = False
        break

endwin()

